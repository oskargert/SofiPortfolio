
# Welcome to Sofia Deneke's portfolio gitlab page
## This is where the magic happens.

# What is Markdown

Markdown is a markup language for creating formatted text according to Wikipedia.
In fact, what you are reading right now is written in markdown. The language has the same capabilities as any word processor you might have used, Word ,google docs, etc.
The difference is the lack of a user interface. Markdown can be written in any text editor, it is written using a markup syntax which you can read more about in this tutorial: https://www.markdownguide.org/basic-syntax/ . It is not as scary and hard as it might seem. Markdown is the way you will be entering text into you portfolio page.

# What files should/can to change?

## Files for the different pages
- Markdown files in the folder _pages
    - about.md
    - contact.md
    - footer.md

### Add image to the about page.
- In the folder "assets/img/other" add your about image. add the full filename of the file to your about markdown file in the field "about_image".

### Add CV to the about page.
- In the folder "assets/cv/" add your CV file, it should be a PDF. add the full filename of the file to your about markdown file in the field "cv_file_name".

## Add a new projects

- Add a new folder into the existing folder _projects, your new folder can be named whatever you want.
- Copy the file "project_examples.markdown" into the folder and rename if to something else, preferibly the same as the folder, but teqnically whatever you want.
- Fill in the empty fields in the markdown file.

## Add images to a project

- In the folder "assets/img/" add a new folder with the same name as the field "image_folder" in the markdown file for the project.
- Add your images into this folder, but first make sure to scale them down to 1920px for the longest side for performance.
- If you want the images to come in a certain order, rename them 1, 2, 3 etc. They are automatically ordered alphabethically.


# Change the look of the page

Before starting to change the actual files, start by altering the webpage using the inspect tool. See tutorial on how to inspect: https://zapier.com/blog/inspect-element-tutorial/ 

## Change using the inspect tool
- When inspecting look for the ":root" element in the "style" section.
- Change the values in the ":root" element and see how the webpage changes in real time.
- Once you have found values you like take note of them, they will dissapear when the page is reloaded.

## Change the styling variables
- In the folder "_sass" open file "variables.scss"
- Change the variables to the values you found using the inspect tool

# Add a new font from google fonts

- Dowload font from google fonts
- Put it in the folder "assets/fonts"
- In folder "_sass" open file "fonts.scss"
- Copy one of the existing "@font-face" tags and change the values of it to the name of you new font and the path where to find it, should be "../fonts/NAME_OF_FONT".
- In folder "_scss" open "variables.scss" 
- Enter the name of the new font to the variables you want. remember to add a couple of backup fonts as well.
- Having problems? Ask Oskar.