---
# Which page this is, dont change.
page: about

# full name of the about image file
about_image: 

#  full name of the cv pdf
cv_file_name: 

# Title to show on the about page
title:  About Me
---


<!-- Here we use the markdown language, see link for a cheat sheet: https://www.markdownguide.org/cheat-sheet/ -->
<!--  Content text to show on the about page is placed here -->

Hi there, that’s me! I’m a Product Design graduate and currently based in Vienna.

What I do:
My past experience includes packaging design, social media content creation, branding, user experience design, and graphic design or illustration freelance projects. Feel free to contact me through the links below!
