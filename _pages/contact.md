---
# Which page this is, dont change.
page: contact
# Title to show on the contact page
title: Contact Me
# Phone number to show on the contact page, has to be in " " if you use + ...
phone:  "+31627879206"
# Email to show on the contact page
email:  "sofiadeneke@gmail.com"
---

<!-- Here we use the markdown language, see link for a cheat sheet: https://www.markdownguide.org/cheat-sheet/ -->
<!--  Content text to show on the contact page is placed here -->
