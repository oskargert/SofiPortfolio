---
# Which page this is, dont change.
page: footer

# Enter Urls to the social media pages you want to show. 
# If left empty no logo is displayed for that media.
facebook_url: 
instagram_url: 
twitter_url: 
linkedin_url: 
tiktok_url: 
youtube_url:
reddit_url:
snapchat_url:
skype_url:
---
