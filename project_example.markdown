---
# Title for the project.
heading: 

# Subheader for the project.
subheading: 

# What year it was done, projects will be ordered showing the newest first.
year: 

# Has to be the name of the image folder for this project
image_folder: 

# How many columns you want the project pictures to be divided into, more pictures, more columns.
num_image_columns: 

# Links to youtube videos for the project. read readme for information on how to get the links. links need to be entered in the following was
# ["link_1", "link_2", "link_3"], you can enter as many links as you want. just enter "[]" if there are no videos.
video_urls: []

---

<!-- Here we use the markdown language, see link for a cheat sheet: https://www.markdownguide.org/cheat-sheet/ -->
<!-- Content text to show for the project is placed here -->

