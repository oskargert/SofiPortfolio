---
# Title for the project.
heading: Magazine Holder

# Subheader for the project.
subheading: Prototyping and Crafting my own Product

# What year it was done, projects will be ordered showing the newest first.
year: 2018

# Has to be the name of the image folder for this project
image_folder: Magazine Holder

# How many columns you want the project pictures to be divided into, more pictures, more columns.
num_image_columns: 2

# Links to youtube videos for the project. read readme for information on how to get the links. links need to be entered in the following was
# ["link_1", "link_2", "link_3"], you can enter as many links as you want. just enter "[]" if there are no videos.
video_urls: []

---


Completed during my bachelor’s course at The Hague University of Applied Sciences, this project focused on the creation of a product inspired by Belgian design duo’s style Muller van Severen. I chose a magazine holder as an object and executed it in a similar tubular and minimal style as the one used by the brand, but with more artificial materials such as acrylic and brighter colours as well. The joint pieces which connect the tubes and give the supporting steel structure a unilinear look were made with a 3D printer, and the pink acrylic piece was bent and moulded with heat. The process ranged from sketching, to computer modelling, rapid prototyping, and crafting the final prototype. 
