---
heading: Packaging Design
subheading: Astroplant’s Plant Incubator Kit
year: 2019
# Has to be the name of the image folder for this project
image_folder: Packaging Design
num_image_columns: 2
video_urls: []
---

My bachelor thesis project included designing and making prototypes for the packaging of non-profit organisation Astroplant’s product. The process ranged from making scaled models of the box and compartments, testing the unboxing and product assembly experience with users, and optimising the use of material so that it could be as sustainable and cost-effective as possible. The result was this casing which protected the electronic components of the product during shipment while communicating the product’s story and categorising the individual pieces in order to make assembly as easy as possible for the user. The final design and instructions were used for the initial shipment of 20 kits.
